import React, { useCallback, useEffect, useState } from 'react';
import { TouchableOpacity } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import axios, { AxiosResponse } from 'axios';

import { Column, Row, Text, ScrollView } from 'app/components/UI';

import { ROUTES } from 'app/navigators/routes';

//name and value set as same, so here we need to put the value how it is saved in db, for the sake of simplicity
const tastesCategories = [
  'music-jazz',
  'horror movies',
  'action',
  'fantasy',
  'dubstep',
  'electronic',
  'Portugal',
  'Germany',
];

type UserType = {
  name: string;
  lastName: string;
};

export const ScreenComponent = function Introduction() {
  const [selectedCategories, setSelectedCategories] = useState<string[]>([]);
  // delete this initial state later if you want
  const [users, setUsers] = useState<UserType[]>([
    {
      name: 'asdfasf',
      lastName: 'asdgas',
    },
  ]);

  useEffect(() => {
    (async () => {
      try {
        // fetching categories, disable for to see mock data
        const response: AxiosResponse<string[]> = await axios.get(`url`);
        setSelectedCategories(response.data);
      } catch (error) {
        setSelectedCategories([]);
      }
    })();
  }, []);

  useEffect(() => {
    (async () => {
      if (!selectedCategories.length) return setUsers([]);
      try {
        // 'users' is object that I'm expecting from BE
        const response: AxiosResponse<{ users: UserType[] }> = await axios.post(
          `url`,
          {
            selectedCategories,
          },
        );
        console.log('====================================');
        console.log(
          selectedCategories,
          response.data.users,
          'categories and users',
        );
        console.log('====================================');
        setUsers(response.data.users);
      } catch (error) {
        setUsers([]);
      }
    })();
  }, [selectedCategories]);

  const onTasteChoosePress = useCallback(
    (taste) => () => {
      setSelectedCategories((prev) => {
        let newArr: string[] = [],
          length = prev.length,
          j = 0;

        while (length > 0) {
          length -= 1;
          if (taste !== prev[length]) newArr[j++] = prev[length];
        }

        if (newArr.length < prev.length) return newArr;
        else return newArr.concat(taste);
      });
    },
    [],
  );

  return (
    <Column stretch>
      <Column as={SafeAreaView} stretch>
        <ScrollView>
          <Column mx={6}>
            <Row style={{ flexWrap: 'wrap' }} mt={10}>
              {tastesCategories.map((val) => (
                <Column
                  as={TouchableOpacity}
                  onPress={onTasteChoosePress(val)}
                  key={val}
                  mr={6}
                  p={3}
                  my={2}
                  borderWidth={1}
                  borderRadius={30}
                  borderColor={
                    selectedCategories.find((taste) => taste === val)
                      ? 'blue'
                      : 'grey'
                  }
                  bg={
                    selectedCategories.find((taste) => taste === val)
                      ? 'blue'
                      : 'transparent'
                  }
                >
                  <Text>{val}</Text>
                </Column>
              ))}
            </Row>
            <Text fontSize={5} my={7} textAlign={'left'}>
              Users
            </Text>
            <Column>
              {users.map(({ name, lastName }) => (
                <Row bg={'grey'} my={4} px={5} py={4} key={name + lastName}>
                  <Column></Column>
                  <Column>
                    <Text>{name}</Text>
                    <Text>{lastName}</Text>
                  </Column>
                </Row>
              ))}
            </Column>
          </Column>
        </ScrollView>
      </Column>
    </Column>
  );
};

export const screen = ROUTES.introduction;
